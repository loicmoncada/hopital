class Nurse extends MedicalStaff {

    public Nurse(String name, String employeeId, int age, String socialSecurityNumber) {
        super(name, employeeId, age, socialSecurityNumber);
    }
    @Override
    public String getRole() {
    return "Nurse";
    }
    @Override
    public void careForPatient(Patient patient) {
        System.out.println("Nurse " + this.name + " cares for " + patient.getName());
    }
}
