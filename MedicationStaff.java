abstract class MedicalStaff extends Person implements Care {
    public String employeeId;

    public MedicalStaff(String name, String employeeId, int age, String socialSecurityNumber) {
        super(name, age, socialSecurityNumber);
        this.employeeId = employeeId;
    }

    abstract String getRole();
   
}
