abstract class Person {
    public String name;
    public int age;
    private String socialSecurityNumber;

    Person(String name, int age, String socialSecurityNumber) {
        this.age = age;
        this.name = name;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }
}