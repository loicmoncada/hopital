
class Doctor extends MedicalStaff {
    public String specialty;

    public Doctor(String specialty, String name, String employeeId, int age, String socialSecurityNumber) {
        super(name, employeeId, age, socialSecurityNumber);
        this.specialty = specialty;
    }
    @Override
    public String getRole() {
    return "doctor";
    }

    @Override
    public void careForPatient(Patient patient) {
        System.out.println("Doctor " + this.name + " cares for " + patient.getName());
    }
}