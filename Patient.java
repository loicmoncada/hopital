import java.util.ArrayList;

class Patient extends Person {
    public String patientId;
    public ArrayList<Illness> illnessList = new ArrayList<>();

    Patient(String patientId, String name, int age, String socialSecurityNumber) {
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
    }

    public void addMedication(Illness illness) {
        this.illnessList.add(illness);
    }

    public String getInfos() {
        String endpoint = "";
        for (Illness illness : illnessList) {
            endpoint += illness.getInfos();
        }
        return "Pour " + this.name + " : " + endpoint + " ";
    }
}