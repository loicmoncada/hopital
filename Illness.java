import java.util.ArrayList;
class Illness {
    public String name;
    public ArrayList<Medication> medicationList = new ArrayList<>();

    Illness(String name) {
        this.name = name;
    }

    public void addMedication(Medication medication) {
        medicationList.add(medication);
    }

    public String getInfos() {
        String endpoint = "";
        for (int i = 0; i < this.medicationList.size(); i++) {
            endpoint += this.medicationList.get(i).getInfo()[0] + " " + this.medicationList.get(i).getInfo()[1] + " ";
        }
        return name + " " + endpoint;
    }
}