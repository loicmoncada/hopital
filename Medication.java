class Medication {
    public String name;
    public String dosage;

    Medication(String name, String dosage) {
        this.name = name;
        this.dosage = dosage;
    }

    public String[] getInfo() {
        return new String[] { this.name, this.dosage };
    }

}