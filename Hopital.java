public class Hopital {
    public static void main(String[] args) {
        // on creer un patient
        Patient coco = new Patient("12", "coco", 24, "12344123");
        // on creer des medocs
        Medication doliprane = new Medication("doliprane", "250g");
        Medication pommade = new Medication("pommade", "20g");
        Medication vitamineC = new Medication("Vitamnie C", "15g");
        // des maladies pas trop majeur
        Illness energyMissing = new Illness("Manque d'énergie");
        Illness malDeTete = new Illness("mal de tete");
        // on ajoute le traitement pour chaque maladie
        energyMissing.addMedication(vitamineC);
        malDeTete.addMedication(pommade);
        malDeTete.addMedication(doliprane);
        // on déclare les maladie de coco
        coco.addMedication(malDeTete);
        coco.addMedication(energyMissing);
        // on creer un docteur
        System.out.println(coco.getInfos());
        Doctor ragab = new Doctor("généraliste", "Ragab", "123", 43, "122525674");
        // ce docteur est le docteur de coco, et nous le dit
        ragab.careForPatient(coco);
        ragab.recordPatientVisit("coco a 17h");
        // on creer une nurse
        Nurse Helene = new Nurse("Helene", "124", 39, "2536531464");
        // cette nurse est la nurse de coco, et nous le dit
        Helene.careForPatient(coco);
        Helene.recordPatientVisit("coco a 18h14");

    }

}